import pytest
import time
import requests
import asyncio
import httpx
import pandas as pd
import backtrader as bt
from datetime import datetime
from tview import TView

URL = 'http://localhost:8158'
# URL = 'http://192.168.0.158:8158'


class Test1:

    def _get(self, endpoint, params={}):
        try:
            res = requests.get(URL + endpoint, params=params)
            return res
        except Exception as e:
            print(e)
        assert False

    @pytest.fixture
    def init(self, mocker):
        self.client = httpx.AsyncClient()
        self.tv = TView()

    def test_api(self):
        print('test')
        res = self._get('/test/').json()
        print(res)
        if res['message'] == 'success':
            return
        assert False

    def send(self, df):
        self.tv.removeSeries()
        self.tv.addCandle(1, df)
        df['Value'] = df['Low'] / 2
        self.tv.addArea(2, df)
        n = len(df)
        for i in range(n):
            if i == n - 1:
                df['Value'][i] = 0.0
            else:
                df['Value'][i] = (df['Close'][i + 1] - df['Close'][i]) * 5
        self.tv.addLine(3, df)
        times = []
        for i in range(n):
            ts = pd.Timestamp(df['Date'][i])
            if ts.weekday() == 3:
                times.append(df['Date'][i])
        self.tv.addMarkers(1, times)
        self.tv.applyScale(1, 'right', 0.05, 0.3)
        self.tv.applyScale(2, 'value', 0.7, 0.05)
        self.tv.applyScale(3, 'value', 0.7, 0.05)
        self.tv.applyOptions(2, {
            'crosshairMarkerRadius': 3,
        })
        self.tv.fitContent()

    @pytest.mark.asyncio
    async def test_draw(self, init):
        print('connect')
        ticker = 'VTI'
        period1 = int(time.mktime(datetime(2019, 1, 1, 23, 59).timetuple()))
        period2 = int(time.mktime(datetime(2021, 8, 20, 23, 59).timetuple()))
        interval = '1d'  # 1d, 1m
        base_url = 'https://query1.finance.yahoo.com/v7/finance'
        query = f'?period1={period1}&period2={period2}&interval={interval}&events=history'
        uri = f'{base_url}/download/{ticker}{query}'
        df = pd.read_csv(uri)
        print(df)
        # df.to_csv(ticker + '.csv', index=False)
        # df = pd.read_csv(ticker + '.csv', index_col='Date')
        # df = pd.read_csv(ticker + '.csv')
        # print(df)
        self.send(df)
        await asyncio.sleep(3)
