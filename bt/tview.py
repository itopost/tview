import requests
import pandas as pd
from datetime import datetime

URL = 'http://localhost:8158'
# URL = 'http://192.168.0.158:8158'


class TView:

    def __init__(self):
        self.markers = []

    def _get(self, endpoint, params={}):
        try:
            res = requests.get(URL + endpoint, params=params)
            return res
        except Exception as e:
            print(e)
        assert False

    def _post(self, endpoint, params={}, data={}):
        try:
            res = requests.post(URL + endpoint, params=params, json=data)
            return res
        except Exception as e:
            print(e)
        assert False

    def test_api(self):
        print('test')
        res = self._get('/test/').json()
        print(res)
        if res['message'] == 'success':
            return
        assert False

    def removeSeries(self):
        self._post('/chart/', data={'cmd': 'removeSeries'})

    def addCandle(self, id, df):
        n = len(df)
        print(n)
        items = []
        for i in range(n):
            item = {
                'time': df['Date'][i],
                'open': df['Open'][i],
                'high': df['High'][i],
                'low': df['Low'][i],
                'close': df['Close'][i],
            }
            items.append(item)
        self._post(
            '/chart/',
            data={
                'cmd': 'addCandlestickSeries',
                'id': id,
                'params': {}
            },
        )
        self._post(
            '/chart/',
            data={
                'cmd': 'setData',
                'id': id,
                'params': items
            },
        )

    def addArea(self, id, df):
        n = len(df)
        print(n)
        items = []
        for i in range(n):
            item = {
                'time': df['Date'][i],
                'value': df['Value'][i],
            }
            items.append(item)
        self._post(
            '/chart/',
            data={
                'cmd': 'addAreaSeries',
                'id': id,
                'params': {
                    'lineWidth': 1,
                }
            },
        )
        self._post(
            '/chart/',
            data={
                'cmd': 'setData',
                'id': id,
                'params': items
            },
        )

    def addLine(self, id, df):
        n = len(df)
        print(n)
        items = []
        for i in range(n):
            item = {
                'time': df['Date'][i],
                'value': df['Value'][i],
            }
            items.append(item)
        self._post(
            '/chart/',
            data={
                'cmd': 'addLineSeries',
                'id': id,
                'params': {
                    'lineWidth': 1,
                }
            },
        )
        self._post(
            '/chart/',
            data={
                'cmd': 'setData',
                'id': id,
                'params': items
            },
        )

    def startMarker(self):
        self.markers = []

    def endMarker(self, id):
        self._post(
            '/chart/',
            data={
                'cmd': 'setMarkers',
                'id': id,
                'params': self.markers
            },
        )
        self.markers = []

    def addBuyMarker(self, dt):
        item = {
            'time': dt,
            'position': 'belowBar',
            'color': '#80ff80',
            'shape': 'arrowUp',
        }
        self.markers.append(item)

    def addSellMarker(self, dt):
        item = {
            'time': dt,
            'position': 'aboveBar',
            'color': 'red',
            'shape': 'arrowDown',
        }
        self.markers.append(item)

    def addMarkers(self, id, times):
        markers = []
        for t in times:
            item = {
                'time': t,
                'position': 'belowBar',
                'color': 'green',
                'shape': 'arrowUp',
            }
            markers.append(item)
        self._post(
            '/chart/',
            data={
                'cmd': 'setMarkers',
                'id': id,
                'params': markers
            },
        )

    def applyOptions(self, id, options):
        self._post(
            '/chart/',
            data={
                'cmd': 'applyOptions',
                'id': id,
                'params': options,
            },
        )

    def fitContent(self):
        self._post('/chart/', data={'cmd': 'fitContent'})

    def applyScale(self, id, scale_id, top, bottom):
        self.applyOptions(
            id,
            {
                'priceScaleId': scale_id,
                'scaleMargins': {
                    'top': top,
                    'bottom': bottom,
                },
            },
        )
