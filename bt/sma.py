import sys
import time
import json
import argparse
from datetime import datetime, timedelta
import numpy as np
import pandas as pd
import backtrader as bt
from backtrader import num2date
from pydantic import BaseModel
from colorama import Fore, init as ColoramaInit
from tview import TView

ColoramaInit(autoreset=True)
FG = Fore.GREEN
FR = Fore.RED
FX = Fore.RESET

tv = TView()


class Position(BaseModel):
    symbol: str
    qty: int
    price: float = 0


class MySizer(bt.Sizer):
    params = dict(value=5000)

    def __init__(self):
        pass

    def _getsizing(self, comminfo, cash, data, isbuy):
        position = self.broker.getposition(data)
        if not position:
            size = self.params.value / data.close[0]
        else:
            size = position.size
        return round(size)


class MyBase(bt.Strategy):

    def __init__(self):
        super().__init__()
        self.last = -1
        self.sells = []
        self.buys = []

    def num2str(self, num):
        return num2date(num).strftime('%Y-%m-%d')

    def log(self, txt, dt=None):
        dt = dt or self.data.datetime[0]
        if self.last == len(self):
            print('                  {}'.format(txt))
        else:
            print('{:>4}, {}, {}'.format(len(self), self.num2str(dt), txt))
            self.last = len(self)

    def add_order(self, orders, position):
        orders.append(position)

    def remove_order(self, orders, position):
        for pos in orders:
            if pos.symbol == position.symbol:
                orders.remove(pos)
                break

    def notify_trade(self, trade):
        pass

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            return
        if order.price is None:
            order.price = 0  # avoid None value
        if order.exectype == bt.Order.Market:
            if order.ordtype == bt.Order.Buy:
                self.remove_order(self.buys, Position(symbol=order.data._name, qty=order.size))
            elif order.ordtype == bt.Order.Sell:
                self.remove_order(self.sells, Position(symbol=order.data._name, qty=order.size))

    def buying(self, symbol):
        data = self.getdatabyname(symbol)
        price = data[0]
        order = self.buy(data=data)
        if order is None:
            self.log(f'{symbol:<5} could not buy')
            return None
        self.log('{}{:<5}{}{:<5} {:>7.2f} x {:>3}'.format(FG, 'Buy:  ', FX, symbol, price, order.size))
        self.add_order(self.buys, Position(symbol=symbol, qty=order.size, price=price))
        dt = num2date(data.datetime[0]).strftime('%Y-%m-%d')
        tv.addBuyMarker(dt)
        return order

    def selling(self, symbol):
        data = self.getdatabyname(symbol)
        price = data[0]
        order = self.close(data=data)
        if order is None:
            self.log(f'{symbol:<5} could not sell')
            return None
        self.log('{}{:<5}{}{:<5} {:>7.2f} x {:>3}'.format(FR, 'Sell: ', FX, symbol, price, order.size))
        self.add_order(self.sells, Position(symbol=symbol, qty=order.size, price=price))
        dt = num2date(data.datetime[0]).strftime('%Y-%m-%d')
        tv.addSellMarker(dt)
        return order


class MyTest(MyBase):
    params = dict(
        pick_num=5,
        trade_days=5,
    )

    def __init__(self):
        super().__init__()
        self.symbols = [data._name for data in self.datas]
        self.symbols = sorted(self.symbols)
        self.ma5 = {}
        self.ma20 = {}
        self.up5 = {}
        self.up20 = {}
        self.ma_diff = {}
        self.ma_sum = {}
        self.highest = {}
        self.lowest = {}
        for sym in self.symbols:
            data = self.getdatabyname(sym)
            self.ma5[sym] = bt.ind.SMA(data, period=5)
            self.ma20[sym] = bt.ind.SMA(data, period=20)
            self.up5[sym] = bt.ind.UpMove(self.ma5[sym]) / data
            self.up20[sym] = bt.ind.UpMove(self.ma20[sym]) / data
            self.ma_diff[sym] = (self.ma5[sym] - self.ma20[sym]) / self.ma20[sym]
            self.ma_sum[sym] = bt.ind.SumN(self.ma_diff[sym], period=20)
            self.highest[sym] = bt.ind.Highest(data, period=20)
            self.lowest[sym] = bt.ind.Lowest(data, period=20)

    def start(self):
        print(f'start {datetime.now().isoformat()}')
        self.picked = set()
        self.info = {}

    def stop(self):
        print('==================================================')
        print('Starting Value  - {:>5.0f}'.format(self.broker.startingcash))
        print('Ending   Value  - {:>5.0f}'.format(self.broker.getvalue()))
        print('==================================================')

    def notify_order(self, order):
        super().notify_order(order)
        if order.status in [order.Submitted, order.Accepted]:
            return
        if order.exectype == bt.Order.Market:
            if order.ordtype == bt.Order.Buy:
                self.info[order.data._name]['price'] = order.executed.price

    def next(self):
        to_remove = set()

        for sym in self.picked:
            data = self.getdatabyname(sym)
            days = len(self) - self.info[sym]['buy_date']
            # date = self.info[sym]['date']
            # price = self.info[sym]['price']
            # chg = (data.close[0] - price) / price * 100.0
            # txt = f'{sym:<5} {price:>7.2f} {data.close[0]:>7.2f} {chg:6.2f}% {date} {days}'
            # self.log(txt)
            if days >= self.p.trade_days:
                self.selling(sym)
                to_remove.add(sym)

        self.picked = self.picked - to_remove

        rank = []
        for sym in self.symbols:
            data = self.getdatabyname(sym)
            if np.isnan(data.close[0]):
                continue
            if (data.close[0] - data.open[0]) / data.open[0] < 0.002:
                continue  # 上昇が0.2%未満
            if 0.05 < (data.close[0] - data.open[0]) / data.open[0]:
                continue  # 5%以上上昇した
            if data.open[0] < self.ma20[sym][0]:
                continue  # 始値が20MAより下
            if data.close[0] < self.ma5[sym][0]:
                continue  # 終値が5MAより下
            if self.up5[sym][0] < 0.0 or self.up20[sym][0] < -0.005:
                continue  # 5MAの傾きが0%以下、20MAの傾きが-0.5%以下
            if self.ma_sum[sym][0] < 0.20:
                continue  # 過去20日の5MAと20MAの差の累計が20%以下
            if self.ma_diff[sym][0] < -0.01 or self.ma_diff[sym][0] > 0.02:
                continue  # 5MAと20MAの差が-1%から2%以内にない
            delta = (self.highest[sym][0] - data.close[0]) / data.close[0]
            if np.isnan(delta):
                continue
            if self.highest[sym][0] != data.close[0] and delta < 0.05:
                continue  # 直近高値まで5%以下
            rank.append((sym, delta * 100.0, data.close[0]))
        rank = sorted(rank, key=lambda x: (x[1], x[2], x[0]), reverse=True)

        pick = []
        for sym in self.picked:
            data = self.getdatabyname(sym)
            pick.append((sym, data.close[0]))
        self.log('Rank: ' + ', '.join('{:<5}{:>7.2f}'.format(rank[i][0], rank[i][1]) for i in range(len(rank))))

        for i in range(len(rank)):
            sym = rank[i][0]
            if sym in self.picked:
                continue
            if len(self.picked) >= self.p.pick_num:
                continue
            self.buying(sym)
            self.info[sym] = {
                'buy_date': len(self),
                'date': self.num2str(self.data.datetime[0]),
            }
            self.picked.add(sym)


def report(strategy):
    ta = strategy.analyzers.ta.get_analysis()
    dd = strategy.analyzers.dd.get_analysis()
    sqn = strategy.analyzers.sqn.get_analysis()
    if ta.total.keys() < {'total', 'open', 'close'}:
        return
    print()
    print(f'Total trades             : {ta.total.closed:}')
    print(f'Total won trade          : {ta.won.total} ({ta.won.total / ta.total.closed * 100:.2f}%)')
    print(f'Total lost trade         : {ta.lost.total} ({ta.lost.total / ta.total.closed * 100:.2f}%)')
    print(f'Age trade length (total) : {ta.len.average:.2f} days')
    print(f'Age trade length (won)   : {ta.len.won.average:.2f} days')
    print(f'Age trade length (lost)  : {ta.len.lost.average:.2f} days')
    print(f'Total trade P&L          : {ta.pnl.net.total:,.0f}')
    print(f'Ave trade P&L (total)    : {ta.pnl.net.average:,.0f}')
    print(f'Age trade P&L (won)      : {ta.won.pnl.average:,.0f}')
    print(f'Age trade P&L (lost)     : {ta.lost.pnl.average:,.0f}')
    print(f'Max trade P&L (won)      : {ta.won.pnl.max:,.0f}')
    print(f'Max trade P&L (lost)     : {ta.lost.pnl.max:,.0f}')
    print(f'Draw Down (book)         : {dd.moneydown:,.0f}')
    print(f'Draw Down (market)       : {dd.max.moneydown:,.0f}')
    print(f'SQN value                : {sqn.sqn:.2f}')
    print()


class PandasData(bt.feeds.DataBase):
    params = (
        ('datetime', None),
        ('open', 'Open'),
        ('high', 'High'),
        ('low', 'Low'),
        ('close', 'Close'),
        ('volume', 'Volume'),
        ('openinterest', None),
    )


def read_barset(ticker, start, end):
    period1 = int(time.mktime(start.timetuple()))
    period2 = int(time.mktime(end.timetuple()))
    interval = '1d'  # 1d, 1m
    base_url = 'https://query1.finance.yahoo.com/v7/finance'
    query = f'?period1={period1}&period2={period2}&interval={interval}&events=history'
    uri = f'{base_url}/download/{ticker}{query}'
    df = pd.read_csv(uri)
    # print(df)
    return df


def sim(start=None, value=2000, num=10):
    global cerebro, res
    cerebro = bt.Cerebro()
    print(f'sim   {datetime.now().isoformat()}')
    end_dt = pd.Timestamp.now(tz='America/New_York')
    end_date = end_dt.strftime('%Y-%m-%d')
    if start is not None:
        start_dt = datetime.strptime(start, '%Y-%m-%d')
        start_date = start_dt.strftime('%Y-%m-%d')
    else:
        start_dt = end_dt - timedelta(days=365 * 2)
        # start_dt = end_dt - timedelta(days=90)
        start_date = start_dt.strftime('%Y-%m-%d')
    print('{} - {}'.format(start_date, end_date))

    symbol = symbols[0]

    df = read_barset(symbol, start_dt, end_dt)

    tv.removeSeries()
    tv.addCandle(1, df)
    tv.startMarker()

    df['Date'] = pd.to_datetime(df['Date'])
    df = df.set_index('Date')
    print(df)

    data = bt.feeds.PandasData(dataname=df, name=symbol,
            fromdate=start_dt, todate=end_dt)
    cerebro.adddata(data)

    # df = stock.barset('SPY', start_date, end_date)
    # size = len(df)  # for checking index length

    # def sub(syms):
    #     for sym in syms:
    #         df = stock.barset(sym, start_date, end_date)
    #         if len(df) < size:
    #             print(sym, 'Not Enough Data')
    #             continue
    #         data = bt.feeds.PandasData(dataname=df, name=sym,
    #                 fromdate=start_dt, todate=end_dt)
    #         cerebro.adddata(data)

    # sub(symbols)

    cerebro.broker.set_cash(value * num * 2)  # to make a room for overshoot
    cerebro.addsizer(MySizer, value=value)
    cerebro.addstrategy(MyTest, pick_num=num)
    cerebro.addanalyzer(bt.analyzers.TradeAnalyzer, _name='ta')
    cerebro.addanalyzer(bt.analyzers.DrawDown, _name='dd')
    cerebro.addanalyzer(bt.analyzers.SQN, _name='sqn')
    print(f'run   {datetime.now().isoformat()}')
    res = cerebro.run(maxcpus=1)
    report(res[0])
    tv.endMarker(1)
    tv.fitContent()


def submit():
    global res
    data = {}
    syms = [item.dict() for item in res[0].buys]
    data['buys'] = syms
    text = json.dumps(data, indent=2)
    print(text, file=sys.stderr)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--start', '-s', help='Backtrade start date.', type=str, default=None)
    parser.add_argument('--value', '-v', help='Value allocation.', type=int, default=2000)
    parser.add_argument('--num', '-n', help='Pick symbol number.', type=int, default=10)
    parser.add_argument('--ticker', '-t', help='Pick ticker symbols.', type=str, default='VTI')

    opt = parser.parse_args()

    symbols = opt.ticker.upper().split(',')
    sim(start=opt.start, value=opt.value, num=opt.num)
    submit()
