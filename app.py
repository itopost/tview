import os
import math
import json
import random
import numpy as np
import pandas as pd
from datetime import datetime
import pytz
import asyncio
import uvicorn
import socketio
from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse
from pydantic import BaseModel
from utils import logger

api = FastAPI()
sio = socketio.AsyncServer(async_mode='asgi', logger=False)
app = socketio.ASGIApp(sio, other_asgi_app=api)
# api.add_middleware(
#     CORSMiddleware,
#     allow_origins=["*"],
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )
loop_task = None


async def event_loop():
    logger.warning('event_loop')
    logger.debug('event_loop')
    logger.error('event_loop')
    await asyncio.sleep(1)
    while True:
        logger.info('clock')
        ts = datetime.now().timestamp()
        sec = 60 - ts % 60  # wait for every 1 min
        if sec <= 60:
            # logger.info(f'sleep {sec}')
            await asyncio.sleep(sec)
    logger.error('event_loop exited')


@api.on_event("startup")
async def startup_loop():
    global loop_task
    logger.info('startup')
    loop_task = asyncio.create_task(event_loop())


@api.on_event("shutdown")
def shutdown_event():
    logger.info('shutdown')


@api.get("/test/")
def test(request: Request):
    logger.info('test endpoint')
    return {'message': 'success', 'client_host': request.client.host}


@api.post("/chart/")
async def chart(request: Request):
    data = await request.json()
    logger.info('chart')
    await sio.emit('message', data)
    return {'message': 'success'}


@sio.event
async def connect(sid, environ):
    logger.info(f'connect {sid}')


@sio.event
async def disconnect(sid):
    logger.info(f'disconnect {sid}')


@sio.on('message')
async def on_message(sid, data):
    logger.info(f'on_message: {data}')


@api.get("/")
def root():
    return FileResponse('www/index.html')


api.mount('/www', StaticFiles(directory='www', html=True), name='static')
