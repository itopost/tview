const chart = LightweightCharts.createChart(document.body, {
  width: 400, height: 300,
});
var series = {
  'chart': chart,
}

var darkTheme = {
  chart: {
    layout: {
      backgroundColor: '#131722',
      lineColor: '#2B2B43',
      textColor: '#D9D9D9',
    },
    watermark: {
      color: 'rgba(0, 0, 0, 0)',
    },
    crosshair: {
      color: '#758696',
    },
    grid: {
      vertLines: {
        color: '#2B2B43',
      },
      horzLines: {
        color: '#363C4E',
      },
    },
  },
  series: {
    topColor: 'rgba(32, 226, 47, 0.56)',
    bottomColor: 'rgba(32, 226, 47, 0.04)',
    lineColor: 'rgba(32, 226, 47, 1)',
  },
};

const lightTheme = {
  chart: {
    layout: {
      backgroundColor: '#FFFFFF',
      lineColor: '#2B2B43',
      textColor: '#191919',
    },
    watermark: {
      color: 'rgba(0, 0, 0, 0)',
    },
    grid: {
      vertLines: {
        visible: false,
      },
      horzLines: {
        color: '#f0f3fa',
      },
    },
  },
  series: {
    topColor: 'rgba(33, 150, 243, 0.56)',
    bottomColor: 'rgba(33, 150, 243, 0.04)',
    lineColor: 'rgba(33, 150, 243, 1)',
  },
};

var themesData = {
  Dark: darkTheme,
  Light: lightTheme,
};

function reportWindowSize() {
  var width = window.innerWidth - 16;
  var height = window.innerHeight - 16;
  console.log('resize', width, height);
  if (chart == null) return;
  chart.resize(width, height);
}

function syncToTheme(theme) {
  console.log('sync theme');
  chart.applyOptions(themesData[theme].chart);
  // areaSeries.applyOptions(themesData[theme].series);
}

window.addEventListener('load', () => {
  console.log('load');
  reportWindowSize();
});
window.addEventListener('resize', () => {
  console.log('resize');
  reportWindowSize();
});

const socket = io();
socket.on("connect", () => {
  // either with send()
  console.log('connected');
  // socket.send("Hello!");
  syncToTheme('Dark');
  // or with emit() and custom event names
  // socket.emit("salutations", "Hello!", { "mr": "john" }, Uint8Array.from([1, 2, 3, 4]));
});

// handle the event sent with socket.send()
socket.on("message", data => {
  console.log(data);
  switch (data['cmd']) {
    case 'applyOptions':
      id = data['id'];
      series[id].applyOptions(data['params']);
      break;
    case 'removeSeries':
      if ('id' in data) {
        id = data['id'];
        chart.removeSeries(series[id]);
        delete series[id];
      } else {
        for (var id in series) {
          if (id != 'chart') {
            chart.removeSeries(series[id]);
            delete series[id];
          }
        }
      }
    case 'addLineSeries':
      id = data['id'];
      series[id] = chart.addLineSeries(data['params']);
      break;
    case 'addAreaSeries':
      id = data['id'];
      series[id] = chart.addAreaSeries(data['params']);
      series[id].applyOptions(themesData['Dark'].series);
      break;
    case 'addCandlestickSeries':
      id = data['id'];
      series[id] = chart.addCandlestickSeries(data['params']);
      break;
    case 'setData':
      id = data['id'];
      series[id].setData(data['params']);
      break;
    case 'setMarkers':
      id = data['id'];
      series[id].setMarkers(data['params']);
      break;
    case 'fitContent':
      chart.timeScale().fitContent();
      break;
    default:
      console.error('unknown key', data['cmd']);
      break;
  }
});
