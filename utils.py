import os
import json
import pytz
from datetime import datetime
from loguru import logger


logger.remove()
logger.add(
    os.sys.stdout, colorize=True,
    format="<green>{extra[time]}</green> | <level>{level}</level> | {message} ({file}:{line})")
logger = logger.patch(lambda record: record['extra'].update(
    time=datetime.now(tz=pytz.timezone('America/New_York')).strftime('%Y-%m-%d %H:%M:%S.%f')))
